var express          = require('express'),
    expressSanitizer = require('express-sanitizer'),
    methodOverride   = require('method-override');
    mongoose         = require('mongoose'),
    bodyParser       = require('body-parser'),
    app              = express();

mongoose.connect('mongodb://localhost/blog_site');
var postSchema = new mongoose.Schema({
    title: String,
    image: {type: String, default: 'https://images.unsplash.com/photo-1519120443186-4620c77c7130?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=047d732d2d8cc8f48f477f5ad6d54f35&auto=format&fit=crop&w=1438&q=80'}, 
    body: String,
    created: {type: Date, default: Date.now}
});

var post = mongoose.model("Post", postSchema)

app.set('view engine', 'ejs');
app.use(express.static('public'));
app.use(expressSanitizer());
app.use(bodyParser.urlencoded({ extended: true })); 
app.use(methodOverride('_method'));

app.get('/', function(req,res) {
    res.redirect('/blogs');
});

app.get('/blogs', function(req, res) {
    post.find({}, function(error, posts) {
        if(error) {
            console.log('We could not find the blogs');
        } else {
            res.render('index', {posts: posts});
        }
    });
});

app.get('/blogs/new', function(req, res) {
    res.render('new');
});

app.post('/blogs', function(req, res) {
    // Sanitize input 
    req.body.blog.body = req.sanitize(req.body.blog.body);
    // Allows user to use our default image
    if(req.body.blog.image === '') {
        var blog = {
            title: req.body.blog.title,
            body: req.body.blog.body
        }
        post.create(blog, function(error, post) { 
            if (error) {
                console.log('We could not create the new blog post! Try again.');
                res.render('new');
            } else {
                res.redirect('/blogs');
            }
        });
    } else {
        post.create(req.body.blog, function(error, post) { 
            if (error) {
                console.log('We could not create the new blog post! Try again.');
                res.render('new');
            } else {
                res.redirect('/blogs');
            }
        });
    }
});

app.get('/blogs/:id', function(req, res) {
    post.findById(req.params.id, function(error, post) {
        if(error) {
            console.log("Could not load show page");
            res.redirect('/blogs');
        } else {
            res.render('show', {post: post});
        }
    });
});

app.get('/blogs/:id/edit', function(req, res) {
    post.findById(req.params.id, function(error, post) {
        if(error) {
            console.log("Could not load edit page");
            res.redirect('/blogs');
        } else {
            res.render('edit', {post: post});
        }
    });
});

app.put('/blogs/:id', function(req, res) {
    // Sanitize input 
    req.body.blog.body = req.sanitize(req.body.blog.body);
    post.findByIdAndUpdate(req.params.id, req.body.blog, function(error, post) {
        if(error) {
            console.log("Could not edit specified blog post");
        }
        res.redirect('/blogs/' + req.params.id);
    });
});

app.delete('/blogs/:id', function(req, res) {
    post.findByIdAndRemove(req.params.id, function(error, post) {
        if(error) {
            console.log("Could not remove specified blog post");
        }
        res.redirect('/blogs');
    });
});

app.listen(3000, function() {
    console.log('Blog Site is listening on port 3000');
});